<?php

namespace mathewparet\LaravelFollowable\Traits;

use BadMethodCallException;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

trait Followable
{
    /**
     * Get all followers for this model
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    // public function followers()
    // {
    //     return $this->morphToMany($this->getFollowerModelClass(), 'followable');
    // }

    public function __call($method, $arguments)
    {
        $data = parent::__call($method, $arguments);

        if($data)
            return $data;

        if($this->isValidFollowerMethod($method))
            return $this->morphToMany($this->getFollowerModelClass($method), 'follower', 'followable');
    }

    private function getFollowerPrefix()
    {
        return config('followable.prefix.follower', 'follower');
    }

    private function isValidFollowerMethod($method)
    {
        return Str::startsWith($method, $this->getFollowerPrefix());
    }

    private function getFollowerModelClass($method)
    {
        $method = substr($method, strlen($this->getFollowerPrefix()));

        return config('followable.models', 'App\\Models').'\\'.Str::singular(Str::ucfirst($method));
    }

    // /**
    //  * Get the follower class
    //  * 
    //  * @return string
    //  */
    // abstract public function getFollowerModelClass();

    /**
     * Add follower to this model
     *
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function addFollower(Model $model)
    {
        return $model->follow($this);
    }

    /**
     * Remove follower from this model
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return bool
     */
    public function removeFollower(Model $model)
    {
        return $model->unfollow($this);
    }

    /**
     * Get the display name of the item being followd
     * 
     * @return string|mixed
     */
    public function getFollowableTitleAttribute()
    {
        return $this->getAttribute('name');
    }

    public static function bootFollowable()
    {
        static::retrieved(function (Model $model) {
            $model->append(['followable_title']);
        });
    }
}
