<?php
namespace mathewparet\LaravelFollowable\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use BadMethodCallException;

trait IsFollower
{
    /**
     * Follows a model
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function follow(Model $model)
    {
        return $model->followers()->save($this);
    }

    /**
     * Unfollows a model
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function unfollow(Model $model)
    {
        return $model->followers()->detach($this);
    }

    /**
     * Get all items being followed
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followings()
    {
        return $this->hasMany(Followable::class);
    }

    public function __call($method, $arguments)
    {
        $data = parent::__call($method, $arguments);

        if($data)
            return $data;
            
        if($this->isValidFollowingMethod($method))
            return $this->morphedByMany($this->getFollowedModelClass($method), 'followable');
    }

    private function isValidFollowingMethod($method)
    {
        return Str::startsWith($method, $this->getFollowingPrefix());
    }

    private function getFollowedModelClass($method)
    {
        $method = substr($method, strlen($this->getFollowingPrefix()));

        return config('followable.models', 'App\\Models').'\\'.Str::singular(Str::ucfirst($method));
    }

    private function getFollowingPrefix()
    {
        return config('followable.prefix.following', 'following');
    }
}