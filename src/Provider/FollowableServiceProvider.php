<?php
namespace mathewparet\LaravelFollowable\Provider;

use Illuminate\Support\ServiceProvider;

class FollowableServiceProvider extends ServiceProvider
{
    const CONFIG_FILE = __DIR__.'/config/followable.php';
    
    public function boot()
    {
        $this->definePublishableComponents();
    }

    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_FILE, 'followable');
    }

    private function definePublishableComponents()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path('followable.php'),
            __DIR__.'/../migrations/create_followables_table.php' => database_path('migrations/'.$this->getTimestamp().'_create_follwables_table.php'),
        ], 'laravel-followable');
    }

    private function getTimestamp()
    {
        return now()->format('Y_m_d_his');
    }
}