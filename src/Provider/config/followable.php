<?php

return [
    /**
     * Base namespace used for models
     */
    'models' => 'App\\Models',

    'prefix' => [
        /**
         * The prefix for following model method
         * 
         * Eg. if prefix is following, then followingAuthors will return all App\Models\Author that is being followed
         */
        'follower' => 'follower',

        /**
         * The prefix for follower model method
         * 
         * Eg. if prefix is follower, then followerAuthors will return all App\Models\Author that is being followed
         */
        'following' => 'following',
    ],
];