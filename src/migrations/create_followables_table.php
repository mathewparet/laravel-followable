<?php
namespace Mathewparet\LaravelFollowable\migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followables', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('user_id');
            $table->string('follower_tyoe');
            $table->bigInteger('follower_id')->unsigned();
            $table->string('followable_type');
            $table->bigInteger('followable_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followables');
    }
};
